clear all; close all; clc; clear memory;

% 1.
Mask = double(imread('F:\Raf_Theunissen\Image_database\BFS\mask_bfs_v2.tif'));

% 2.
Mask_buf        = Mask;     
Mask_buf(Mask<0)= 0; %Negative values in Mask indicate that these are reflections and we do not need to take them into account
diff_y          = zeros(size(Mask_buf));
diff_y(2:end-1,:) = (diff(Mask_buf,2,1));
diff_x          = zeros(size(Mask_buf));
diff_x(:,2:end-1) = (diff(Mask_buf,2,2));

Edges_buf       = zeros(size(diff_x));
Diff_buf        = diff_y+diff_x;
ind             = find(Diff_buf<0);
Edges_buf(ind)  = 1; % -> Edges are 1 if there was a difference in gray-scale

% 3.
Edges           = imdilate(Edges_buf,ones(7));

% 4.    
ind             = find(Edges);

% 5.
for i=1:320
    
    if i<1000; nz=''; end;
    if i<100; nz='0'; end;
    if i<10; nz='00'; end;
    
    file_a = ['F:\Raf_Theunissen\Image_database\BFS\image_8bit_',nz,num2str(i),'a.tif'];
    file_b = ['F:\Raf_Theunissen\Image_database\BFS\image_8bit_',nz,num2str(i),'b.tif'];
    
    A      = double(imread(file_a));
    B      = double(imread(file_b));
    
    A(ind) = 255;
    B(ind) = 255;
    
    file_a_new = ['F:\Raf_Theunissen\Image_database\BFS\image_8bit_adj_',nz,num2str(i),'a.tif'];
    file_b_new = ['F:\Raf_Theunissen\Image_database\BFS\image_8bit_adj_',nz,num2str(i),'b.tif'];
    
    imwrite(uint8(A),file_a_new,'tif');
    imwrite(uint8(B),file_b_new,'tif');
    
end;
    
