%script to create ensemble of 2x2 vortex array images

%flow field definition
flowType		= 101; %vortex flowType
NvortX			= 2; %number of vortices in the x direction
NvortY			= 2; %number of vortices in the y direction
maxDisplacement = 15; 
phaseShiftL		= 0; %phase shift of the vortex array left-right
phaseShiftD		= 0; %phase shift of the vortex array up-down

flowArgs		= [NvortX,NvortY,maxDisplacement,phaseShiftL,phaseShiftD];

%image properties
seedDens	= 0.01;
particleDia = 3;
sigNoise	= 0;
meanNoise	= 0;
imgDim		= [1000,1000];

imgArgs		= [seedDens, particleDia, sigNoise, meanNoise];

%number of images to create
Nimages = 100;

%define output location 
outFolder	= 'C:\Users\me12288\Local Documents\PhD - Local\images\PIV_library\Varray2x2';
outFile		= 'Varray2x2_%03d%c';

%create images
createImageEnsemble(flowType, flowArgs, imgDim, imgArgs, Nimages, outFolder, outFile);



